import human.Human;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Human h1 = new Human("Ivan", "Ivanov", "Ivanovich",
                "male", "20.04.1999", 21);
        Human h2 = new Human("Alex", "Alexox", "Alexandrovich",
                "male", "11.02.2005", 15);
        Human h3 = new Human("Sergey", "Sergeev", "Sergeyevich",
                "male", "07.09.1970", 50);
        Human patternHuman = new Human("Alex", "Alexox", "Alexandrovich",
                "male", "11.02.2005", 15);

        List<Human> humansArr = new ArrayList<>();
        humansArr.add(h1);
        humansArr.add(h2);
        humansArr.add(h3);

        if (humansArr.contains(patternHuman)) System.out.println("Match found");


        for (Human h : humansArr) {
            String lastNameLowerCase = h.getLastName().toLowerCase();
            if (h.getAge() < 21 && lastNameLowerCase.startsWith("а") ||
                    lastNameLowerCase.startsWith("б") ||
                    lastNameLowerCase.startsWith("в") ||
                    lastNameLowerCase.startsWith("г")) {
                System.out.println(h.toString());
            }
        }

        List<Human> humansArrDuplicated = new ArrayList<>();
        int matches = 0;

    }
}
