package human;

import java.util.Objects;

public class Human {
    private String firstName,
                    lastName,
                    middleName,
                    gender,
                    dateOfBirth;
    private int age;


    public Human(String firstName, String lastName, String middleName,
                 String gender, String dateOfBirth, int age) {
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.middleName  = middleName;
        this.gender      = gender;
        this.dateOfBirth = dateOfBirth;
        this.age         = age;
    }

    public int getAge() {
        return age;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return firstName.equals(human.firstName) &&
                lastName.equals(human.lastName) &&
                middleName.equals(human.middleName) &&
                gender.equals(human.gender) &&
                dateOfBirth.equals(human.dateOfBirth) &&
                age == human.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middleName, gender, dateOfBirth, age);
    }

    @Override
    public String toString() {
        return firstName + " " +
                lastName + " " +
                middleName + " " +
                gender + " " +
                dateOfBirth + " " +
                age;
    }
}
